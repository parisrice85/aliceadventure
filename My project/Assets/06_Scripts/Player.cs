using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using MoreMountains.Feedbacks;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Player : MonoBehaviour
{
    private Animator _anim;
    private Rigidbody _rigidbody;
    private static readonly int Animation = Animator.StringToHash("Animations");
    
    public bool onAttack;

    public Transform busterSword;
    public Transform idleTransform;

    [Tooltip("Virtual Camera Position")]
    public CinemachineVirtualCamera[] virCams;

    [Tooltip("Player Attack Effect Feedback")]
    public MMF_Player atkFeedback;
    [Tooltip("Enemy Damaged Effect Feedback")]
    public MMF_Player damFeedback;
    
    public GameObject attackNote;
    private Vector3 _busterOriginPos;
    private Quaternion _busterOriginRot;
    
    // Start is called before the first frame update
    void Awake()
    {
        _anim = GetComponent<Animator>();
        _rigidbody = GetComponent<Rigidbody>();
        _busterOriginPos = busterSword.position;
        _busterOriginRot = busterSword.rotation;
    }

    public void Animations(int animNum)
    {
        _anim.SetInteger(Animation,animNum);
    }

    public void AnimIdle()
    {
        _anim.SetInteger(Animation,0);
    }

    public void SkillFocus()
    {
        virCams[1].Priority = 5;
        transform.position = new Vector3(1, 2.5f, 4);
    }

    public void SkillEffect()
    {
        virCams[1].Priority = 0;
        virCams[2].Priority = 5;
    }

    public void CameraIdle()
    {
        for (int i = 0; i < virCams.Length; i++)
        {
            if (i != 0)
            {
                virCams[i].Priority = 0;
            }
            else
            {
                virCams[i].Priority = 1;
            }
        }

        transform.position = Vector3.zero;
    }

    public void AttackFeedback()
    {
        atkFeedback.PlayFeedbacks();
        damFeedback.PlayFeedbacks();
        // 어택 파티클
        // 적 피격 파티클
        // 콤보 증가
        // 데미지
        // 사운드
    }
    
    public void DamageFeedback()
    {
        
    }
}
