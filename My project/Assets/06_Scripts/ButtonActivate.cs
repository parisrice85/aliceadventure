using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MoreMountains.Feedbacks;

public class ButtonActivate : MonoBehaviour
{
    private Slider _slider;
    private Button _button;
    private ColorBlock _buttonColors;
    
    public MMF_Player MyPlayer;
    public float coolTime;
    public int attackNum;

    public float SliderValue => _slider.value;

    void Awake()
    {
        _button = gameObject.AddComponent<Button>();
        _slider = GetComponentInParent<Slider>();
        _button.onClick.AddListener(AttackButton);
        ButtonColorSet();
        
        _slider.value = 0;
        _button.interactable = false;
        
        StartCoroutine(AttackCor());
    }

    void ButtonColorSet()
    {
        _buttonColors = _button.colors;
        Color color = _buttonColors.disabledColor;
        color.r = 0.5f;
        color.g = 0.5f;
        color.b = 0.5f;
        _buttonColors.disabledColor = color;
        _button.colors = _buttonColors;
    }

    private void Update()
    {
        _slider.value += Time.deltaTime * coolTime;
    }

    IEnumerator AttackCor()
    {
        yield return new WaitUntil(() => _slider.value >= 1);
        _button.interactable = true;
        // 버튼 깜빡임 애니메이션 실행
    }

    private void AttackButton()
    {
        // 버튼에 따른 공격 실행
        _slider.value = 0;
        _button.interactable = false;
        if(MyPlayer != null)
            Feedback();
        GameManager.Instance.player.Animations(attackNum);
        StartCoroutine(AttackCor());
    }

    private void Feedback()
    {
        MyPlayer.Direction = MMFeedbacks.Directions.BottomToTop;
        MyPlayer.PlayFeedbacks();
    }
}
